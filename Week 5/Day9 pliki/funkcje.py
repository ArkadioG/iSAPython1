def funkcja():
    pass

# 1 argument wymagany
def funkcja1(imie):
    print(imie)

# 2 wymagane argumenty
def funkcja2(imie, nazwisko):
    print(imie, nazwisko)

# argument wymagany i domyślny
def funkcja3(imie, nazwisko='Iksinski'):
    print(imie, nazwisko)

# argument domyślny
def funkcja4(nazwisko='Iksinski'):
    print(nazwisko)

# tutaj jest błąd, argumenty doyślne muszą być
# zawsze po argumentach wymaganycyh
# def funkcja4(nazwisko='Iksinski', imie):
#     print(nazwisko, imie)


def star_wars(name):
    return 'Jedi {}'.format(name)

arek_jedi = star_wars('Arek')

def star_wars1(name):
    jedi = 'Jedi {}'.format(name)
    return jedi

# zmienna jedi jest zmienną lokalną wewnątrz funkcji
print(jedi)


def star_wars1(name):
    '''
    Trenuje kandydata w Jedi
    '''
    # tutaj korzystamy ze zmiennej globalnej
    global arek_jedi
    arek_jedi = 'Jedi {}'.format(name)
    return arek_jedi






