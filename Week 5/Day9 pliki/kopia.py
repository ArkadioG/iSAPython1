# definiujemy sobie moduł, z jakimiś funkcjami, zmiennymi

# definiujemy funkcję
def deepcopy(wyraz):
    return str(wyraz).capitalize()

# definiujemy funkcję main, nasz główny program w tym module/pliku
# chcemy wykonywać ją tylko gdy uruchamiamy nasz moduł/plik 'kopia.py'
# ale nie chcemy aby ta funkcja wykonywała swój kod, gdy będzie importowana
# do innych modułów
def main():
    print(deepcopy('ola w module kopia'))

# dzięki temu warunkowi Python będzie wywoływał funkcję main
# zawsze gdy uruchomimy nasz plik 'kopia.py' bezpośrednio tzn.
# gdy uruchomimy w jakimś IDE, lub gdy skorzytsamy z wiersza poleceń:
# c:/> python kopia.py
if __name__ == '__main__':
    main()


