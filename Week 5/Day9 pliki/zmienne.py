
# int
numer = 23

# double
waga = 23.5

# string - immutable
imie = 'Magdalena'

# bool
True
False

# indeksowanie
imie[2]

len(imie)

imie[len(imie)-1]

# slicing
imie[0:9:2]
imie[0::2]
imie[::2]

# odwrocony string
imie[::-1]

"Moje imie: {} {} {}".format(imie)

# operatory
# +, -, *, **, /, //, %
# =

# +=, -+= itd.











