# boolean
True
False

# and
# or
# not

# True
True and True

# False
True and False

# True
True or True

# True
False or True

# True
not True or True

# False
not (True or True)

# instr wewnątrz wykonane. tylko gdy warunek == True
if True:
    pass
elif True:
    pass
elif False:
    pass
else:
    pass


if 6 > 5:
    print("LOL")
if 5< 7:
    print("WOW")























