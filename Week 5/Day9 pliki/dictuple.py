slownik = {1:'ola', 'dwa':3}

x = slownik[1]
y = slownik['dwa']

# dodajemy element do słownika
slownik.update({'nowy':[1,2,3,4,5]})

print(slownik)

# tu są wszystkie klucze
slownik.keys()
# tu są wszystkie wartości
slownik.values()

# jednoelementowy tuple
krotka = 1,
print(krotka)
print(type(krotka))

krotka2 = (1,2)
print(krotka2)

